﻿namespace WhilePractice
{
    public static class Task4
    {
        public static double SumSequenceElements(int n)
        {
            double sum = 0;
            for (int i = 1; i <= n; i++)
            {
                sum += 1 / (((2 * (double)i) + 1) * ((2 * (double)i) + 1));
            }

            return sum;
        }
    }
}
