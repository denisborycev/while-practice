﻿namespace WhilePractice
{
    public static class Task2
    {
        public static double SumSequenceElements(int n)
        {
            double sum = 0;
            for (int i = 1; i <= n; i++)
            {
                sum += Math.Pow(-1, i + 1) * 1 / ((i + 1) * (double)i);
            }

            return sum;
        }
    }
}
