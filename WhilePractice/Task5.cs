﻿namespace WhilePractice
{
    public static class Task5
    {
        public static double GetSequenceProduct(int n)
        {
            double sum = 1;
            for (int i = 1; i <= n; i++)
            {
                sum *= 1d + (1d / (i * (double)i));
            }

            return sum;
        }
    }
}
